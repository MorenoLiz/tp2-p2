package tp2;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class TestDiccOtro {

	private Dicc1<String, String> diccA, diccB,diccNull;
	private Dicc2<Integer, String> dicc21, dicc22,dicc23;	

	@Before
	public void setUp() 
	{
		diccNull = null;
		diccA = new Dicc1<String, String>();
		diccB = new Dicc1<String, String>();
		dicc21 = new Dicc2<Integer, String>();
		dicc22 = new Dicc2<Integer, String>();
		dicc23 = new Dicc2<Integer,String>();
		
		
		diccA.definir("dos", "Esto es un dos");
		diccA.definir("cuatro", "Esto es un cuatro");
		diccA.definir("uno", "Esto es un uno"); 

		//igual que diccA pero en otro orden 
		diccB.definir("uno", "Esto es un uno");
		diccB.definir("cuatro", "Esto es un cuatro");
		diccB.definir("dos", "Esto es un dos");
		
		dicc21.definir(1, "Esto es un uno"); 
		dicc21.definir(2, "Esto es un dos");
		
		//igual que dicc21 pero en otro orden 
		dicc22.definir(2, "Esto es un dos");
		dicc22.definir(4, "Esto es un cuatro");
		dicc22.definir(1, "Esto es un uno"); 
		
		dicc23.definir(4, "Esto es un cuatro");
		dicc23.definir(2, "Esto es un dos");
		dicc23.definir(5, "Esto es un cinco");
		dicc23.definir(1, "Esto es un uno");
	}

	@Test
	public void testDiccEqualsNull()
	{
		assertFalse(diccA.equals(diccNull));
		assertFalse(dicc22.equals(diccNull));
	}
	
	@Test
	public void testDiccEquals()
	{
		assertFalse(dicc21.equals(dicc22)); 
		assertTrue(diccA.equals(diccB));
		assertFalse(diccB.equals(dicc21));
		assertFalse(dicc22.equals(dicc23));
	}
	
	@Test
	public void testDiccTamano()
	{
		dicc23.eliminar(1);
		assertEquals(2,dicc21.tamano());
		assertEquals(3,diccA.tamano());
		assertEquals(3, dicc23.tamano());
	}
}

