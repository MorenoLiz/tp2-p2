package tp2;
import java.util.Iterator;
import java.util.LinkedList;

public class Dicc2<C, S> implements Dicc<C,S>
{
	LinkedList<TuplaDicc<C,S>> Diccionario;
	
	Dicc2()
	{
		Diccionario= new LinkedList<TuplaDicc<C,S>>();
	}
	
	@Override
	public S obtener(C clave)
	{
		S significado = null;
		if(!pertenece(clave))
			throw new RuntimeException("no existe la clave");
		for(TuplaDicc<C,S>  td: Diccionario)
		{
			if(clave.equals(td.e1))
				significado=td.e2;
		}
		return significado;
	}

	@Override
	public void definir(C clave, S significado)
	{
		eliminar(clave);
		
		Diccionario.add(new TuplaDicc<C,S>(clave, significado));
	}

	@Override
	public boolean pertenece(C clave) 
	{
		return Diccionario.contains(new TuplaDicc<C,S>(clave, null));
	}

	private boolean pertenece(C clave, S significado)
	{
		if(pertenece(clave))
			for(TuplaDicc<C,S> td: Diccionario)
				if(td.e2.equals(significado))
					return true;
		return false;
	}
	
	@Override
	public void eliminar(C clave)
	{
		Diccionario.removeIf(td -> td.e1.equals(clave));
	}

	@Override
	public Iterator<C> iterator() 
	{
		LinkedList<C> claves= new LinkedList<C>();
		
		for(TuplaDicc<C,S> td: Diccionario)
		{
			claves.add(td.e1);
		}
		return claves.iterator();
	}
	
	@Override
	public boolean equals(Object otroDicc)
	{
		if(otroDicc==null) return false;
		boolean ret= true;
		
		if (!(otroDicc instanceof Dicc))
			return false;
		Dicc<C,S> param= (Dicc<C,S>) otroDicc;

		if( tamano()!= param.tamano())
			return false;
		
		Iterator<C> it =param.iterator();
		
		while(it.hasNext())
		{
			C aux = it.next();
			ret= ret && pertenece(aux, param.obtener(aux));
		}
		return ret;
	}
	
	public String toString(Object objeto)
	{
		return Diccionario.toString();
	}
	
	@Override
	public int tamano() 
	{
		int cantClaves=0;
		for(TuplaDicc<C,S> td: Diccionario)
		{
			cantClaves=cantClaves+1;
		}
		return cantClaves;
	}
}
